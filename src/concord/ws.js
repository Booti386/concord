let logger = await use('concord.logger')

let Enum = await use('concord.types.Enum')

let ws = await ready('concord.ws')

class CloseEventCode extends Enum
{
	static NORMAL_CLOSURE = 1000
	static GOING_AWAY = 1001
	static PROTOCOL_ERROR = 1002
	static UNSUPPORTED_DATA = 1003
	static NO_STATUS_RECEIVED = 1005
	static ABNORMAL_CLOSURE = 1006
	/* ... */
}
xport(CloseEventCode)

class Ws
{
	ws
	url
	autoReconnectTimeout
	_autoReconnectForceDisable

	constructor(url, auto_reconnect_timeout)
	{
		url ??= null
		auto_reconnect_timeout ??= 3

		this.ws = null
		this.url = url
		this.autoReconnectTimeout = auto_reconnect_timeout
		this._autoReconnectForceDisable = false
	}

	boot(url)
	{
		this.url = url ?? this.url

		this.connect()
	}

	isConnected()
	{
		return this.ws !== null
	}

	connect()
	{
		this.ws = new WebSocket(this.url)

		this.ws.addEventListener('open', ev => this.onWsOpen(ev))
		this.ws.addEventListener('message', ev => this.onWsMsg(ev))
		this.ws.addEventListener('error', ev => this.onWsError(ev))
		this.ws.addEventListener('close', ev => this.onWsClose(ev))
	}

	disconnect(auto_reconnect)
	{
		auto_reconnect ??= false

		if (!auto_reconnect)
			this._autoReconnectForceDisable = true

		this.ws.close()
	}

	onWsOpen(ev)
	{
		logger.debug('Ws.onWsOpen()')
	}

	onWsMsg(ev)
	{
		logger.debug('Ws.onWsMsg(): ', ev.data)
	}

	onWsError(ev)
	{
		logger.debug('Ws.onWsError(): ', ev)
	}

	onWsClose(ev)
	{
		let close_msg = ws.CloseEventCode.asString(ev.code)
		let auto_reconnect_timeout = this.autoReconnectTimeout

		let reason = ev.reason
		if (reason !== '')
			reason = `: ${reason}`

		logger.debug(`Ws.onWsClose(): ${close_msg}${reason}`)
		this.ws = null

		if (!this._autoReconnectForceDisable
				&& auto_reconnect_timeout > 0)
		{
			window.setTimeout(() => {
				logger.debug(`Ws.onWsClose(): Reconnecting in ${auto_reconnect_timeout} seconds...`)
				window.setTimeout(() => this.connect(), auto_reconnect_timeout * 1000)
			}, 0)
		}

		this._autoReconnectForceDisable = false
	}
}
xport(Ws)
