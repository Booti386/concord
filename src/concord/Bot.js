let concord = await use('concord')

let Gw = await use('concord.gw.Gw')
let Rest = await use('concord.rest.Rest')

// Join URL: https://discord.com/api/oauth2/authorize?response_type=code&client_id=xxxxx&scope=bot&redirect_uri=https://myuri.com/
class Bot
{
	token
	gw
	rest

	constructor(token)
	{
		this.token = token
		this.gw = null
		this.rest = null
	}

	boot()
	{
		this.gw ??= new Gw(this.token)
		this.rest ??= new Rest(this.token)

		this.gw.boot()
	}
}
