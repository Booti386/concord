let Enum = await use('concord.types.Enum')

let logger = await ready('concord.logger')

class LogLevel extends Enum
{
	static NONE = 0
	static ERROR = 1
	static WARN = 2
	static LOG = 3
	static INFO = 4
	static DEBUG = 5
}
xport(LogLevel)

let loglevel = LogLevel.WARN

function getLogLevel()
{
	return loglevel
}
xport(getLogLevel)

function setLogLevel(lvl)
{
	loglevel = lvl
}
xport(setLogLevel)

function error(...args)
{
	if (loglevel < LogLevel.ERROR)
		return

	console.error(...args)
}
xport(error)

function warn(...args)
{
	if (loglevel < LogLevel.WARN)
		return

	console.warn(...args)
}
xport(warn)

function log(...args)
{
	if (loglevel < LogLevel.LOG)
		return

	console.log(...args)
}
xport(log)

function info(...args)
{
	if (loglevel < LogLevel.INFO)
		return

	console.info(...args)
}
xport(info)

function debug(...args)
{
	if (loglevel < LogLevel.DEBUG)
		return

	console.debug(...args)
}
xport(debug)
