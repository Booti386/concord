let logger = await use('concord.logger')
let util = await use('concord.util')

let rest = await ready('concord.rest')

class MsgAutoConsume
{
	static STAGE_FILE = 0
	static STAGE_TITLE = 1
	static STAGE_MSG = 2
	static STAGE_FIELDS = 3
	static STAGE_FOOTER = 4
	static STAGE_DONE = 5

	stage
	abbrev
	title
	msg
	fields
	footer
	file

	constructor(abbrev, title, msg, fields, footer, file)
	{
		this.stage = MsgAutoConsume.STAGE_FILE
		this.abbrev = abbrev ?? ''
		this.title = title ?? ''
		this.msg = msg ?? ''
		this.fields = fields ?? []
		this.footer = footer ?? ''
		this.file = file ?? null
		this.toBin()
	}

	toBin(ctx)
	{
		this.abbrev = util.str2bin(this.abbrev)
		this.title = util.str2bin(this.title)
		this.msg = util.str2bin(this.msg)
		this.fields = this.fields.map(f => ({
			name: util.str2bin(f.name ?? ''),
			value: util.str2bin(f.value ?? ''),
			inline: f.inline
		}))
		this.footer = util.str2bin(this.footer)
		return this
	}

	toStr(ctx)
	{
		this.abbrev = util.bin2str(this.abbrev)
		this.title = util.bin2str(this.title)
		this.msg = util.bin2str(this.msg)
		this.fields = this.fields.map(f => ({
			name: util.bin2str(f.name ?? ''),
			value: util.bin2str(f.value ?? ''),
			inline: f.inline
		}))
		this.footer = util.bin2str(this.footer)
		return this
	}

	computeRemLength(ctx)
	{
		let len = this.title.length
			+ this.msg.length
			+ this.fields.reduce((acc, f) => acc
				+ f.name.length
				+ f.value.length, 0)
			+ this.footer.length
		if (len >= Rest.LIMIT_EMBED)
			return 0

		return Rest.LIMIT_EMBED - len
	}

	consume()
	{
		let acc = new MsgAutoConsume()
		acc.abbrev = this.abbrev

		if (this.stage === MsgAutoConsume.STAGE_FILE)
		{
			acc.file = this.file

			this.stage = MsgAutoConsume.STAGE_TITLE
		}

		let limit = acc.computeRemLength()

		if (this.stage === MsgAutoConsume.STAGE_TITLE)
		{
			if (this.title.length > 0)
			{
				let lim = limit
				if (lim > Rest.LIMIT_EMBED_TITLE)
					lim = Rest.LIMIT_EMBED_TITLE

				acc.title = util.binStrTrunc(this.title, lim, this.abbrev)

				limit = acc.computeRemLength()
			}

			this.stage = MsgAutoConsume.STAGE_MSG
		}

		if (this.stage === MsgAutoConsume.STAGE_MSG)
		{
			if (this.msg.length > 0)
			{
				let lim = limit
				if (lim > Rest.LIMIT_EMBED_DESCRIPTION)
					lim = Rest.LIMIT_EMBED_DESCRIPTION

				acc.msg = util.binStrTrunc(this.msg, lim)

				if (acc.msg.length < this.msg.length)
				{
					let spl_idx = acc.msg.lastIndexOf('\n')

					if (spl_idx < acc.msg.length / 2)
						spl_idx = acc.msg.lastIndexOf(' ')

					if (spl_idx >= acc.msg.length / 2)
						acc.msg = acc.msg.substring(0, spl_idx)
				}

				this.msg = this.msg.substring(acc.msg.length)

				limit = acc.computeRemLength()
			}

			if (this.msg.length === 0)
				this.stage = MsgAutoConsume.STAGE_FIELDS
		}

		if (this.stage === MsgAutoConsume.STAGE_FIELDS)
		{
			if (this.fields.length > 0)
			{
				// TODO
				acc.fields = this.fields
				this.fields = []

				limit = acc.computeRemLength()
			}

			this.stage = MsgAutoConsume.STAGE_FOOTER
		}

		if (this.stage === MsgAutoConsume.STAGE_FOOTER)
		{
			if (this.footer.length > 0)
			{
				let len = this.footer.length
				if (len > Rest.LIMIT_EMBED_FOOTER_TEXT)
					len = Rest.LIMIT_EMBED_FOOTER_TEXT

				if (len <= limit)
				{
					acc.footer = util.binStrTrunc(this.footer, len, this.abbrev)
					this.footer = ''

					limit = acc.computeRemLength()
				}
			}

			if (this.footer.length === 0)
				this.stage = MsgAutoConsume.STAGE_DONE
		}

		return acc.toStr()
	}
}

class Rest
{
	static API_VER = 8
	static API_ROOT = 'https://discord.com/api'

	static LIMIT_EMBED = 6000
	static LIMIT_EMBED_TITLE = 256
	static LIMIT_EMBED_DESCRIPTION = 2048
	static LIMIT_EMBED_FOOTER_TEXT = 2048
	static LIMIT_EMBED_AUTHOR_NAME = 256
	static LIMIT_EMBED_FIELDS = 25
	static LIMIT_EMBED_FIELD_NAME = 256
	static LIMIT_EMBED_FIELD_VALUE = 1024

	token

	constructor(token)
	{
		this.token = token
	}

	send(method, url, data, headers)
	{
		data ??= null
		headers ??= new Headers()

		headers.append('Authorization', `Bot ${this.token}`)

		let params = {
			method: method,
			headers: headers
		}

		if (data !== null)
			params.body = data

		let send = async (url, params) => {
			let ans = await fetch(`${Rest.API_ROOT}/v${Rest.API_VER}${url}`, params)
			if (ans.status !== 429)
				return ans

			let data = await ans.json()
			return new Promise(resolve => {
				let delay = data.retry_after * 1000
				logger.warn(`Rest.send(): Rate limit reached: Delaying send after ${delay} ms...`)
				window.setTimeout(() => resolve(send(url, params)), delay)
			})
		}
		return send(url, params)
	}

	createEmbedMessage(id_chan, title, msg, fields, footer, file /* : File */)
	{
		title ??= ''
		msg ??= ''
		fields ??= null
		footer ??= null
		file ??= null

		let params = {
			embed: {
				title: title,
				description: msg
			}
		}

		if (fields !== null)
			params.embed.fields = fields

		if (footer !== null)
		{
			params.embed.footer = {
				text: footer
			}
		}

		let headers = new Headers()
		let data = JSON.stringify(params)

		if (file !== null)
		{
			// Do not explicitly set the content-type to let the browser do it properly

			let d = new FormData()
			d.set('payload_json', data)
			d.set('file', file)
			data = d
		}
		else
			headers.set('Content-Type', 'application/json')

		return this.send('POST', `/channels/${id_chan}/messages`, data, headers)
	}

	async createEmbedMessageAutoSplit(id_chan, title, msg, fields, footer, file /* : File */)
	{
		let ctx = new MsgAutoConsume('…', title, msg, fields, footer, file)
		let last_reply = null

		while (true)
		{
			let acc = await ctx.consume()

			last_reply = await this.createEmbedMessage(id_chan,
					acc.title,
					acc.msg,
					acc.fields,
					acc.footer,
					acc.file)

			if (ctx.stage === MsgAutoConsume.STAGE_DONE)
				break
		}

		return last_reply
	}

	createReaction(id_chan, id_msg, emoji)
	{
		emoji = emoji.replace(/^<a?:(.+)>$/, '$1')

		return this.send('PUT', `/channels/${id_chan}/messages/${id_msg}/reactions/${emoji}/@me`)
	}
}
xport(Rest)
