let logger = await use('concord.logger')
let sdp = await use('concord.sdp')
let util = await use('concord.util')
let ws = await use('concord.ws')

let Bitfield = await use('concord.types.Bitfield')
let Enum = await use('concord.types.Enum')

let voice = await ready('concord.voice')

class Op extends Enum
{
	static IDENT = 0
	static SELECT_PROTO = 1
	static READY = 2
	static HEARTBEAT = 3
	static SESSION_DESCR = 4
	static SPEAKING = 5
	static HEARTBEAT_ACK = 6
	static RESUME = 7
	static HELLO = 8
	static RESUMED = 9
	static CLIENT_CONNECT = 12
	static CLIENT_DISCONNECT = 13
}
xport(Op)

class CloseEventCode extends ws.CloseEventCode
{
	static UNK_OPCODE = 4001
	static NOT_AUTH = 4003
	static AUTH_FAILED = 4004
	static ALREADY_AUTH = 4005
	static INVAL_SESSION = 4006
	static SESSION_TIMEOUT = 4009
	static SRV_NOT_FOUND = 4011
	static UNK_PROTO = 4012
	static DISCONNECTED = 4014
	static VOICE_SRV_CRASHED = 4015
	static UNK_ENC_MODE = 4016
}
xport(CloseEventCode)

class Speaking extends Bitfield
{
	static NONE = 0
	static MICROPHONE = 1 << 0
	static SOUNDSHARE = 1 << 1
	static PRIORITY = 1 << 2
}
xport(Speaking)

class Direction extends Bitfield
{
	static NONE = 0
	static IN = 1 << 0
	static OUT = 1 << 1
}
xport(Direction)

class Gw extends ws.Ws
{
	static API_VER = 5

	direction

	heartbeatInterval
	heartbeatIntervalId
	endpoint
	idServer
	idUser
	idSession
	token
	rpc
	localSdp
	remoteSdp
	audioInCtx
	audioOutCtx
	audioInMediaStream
	audioOutMediaStream

	constructor(endpoint, id_server, id_user, id_session, token, auto_reconnect_timeout)
	{
		super(null, auto_reconnect_timeout)

		this.direction = voice.Direction.NONE

		this.heartbeatInterval = null
		this.heartbeatIntervalId = null
		this.endpoint = endpoint
		this.idServer = id_server
		this.idUser = id_user
		this.idSession = id_session
		this.token = token
		this.rpc = null
		this.localSdp = null
		this.remoteSdp = null
		this.audioCtx = null
		this.audioInMediaStream = null
		this.audioOutMediaStream = null
	}

	boot()
	{
		let endpoint = this.endpoint.replace(/:80$/, '')

		super.boot(`wss://${endpoint}/?v=${Gw.API_VER}`)
	}

	send(op, data)
	{
		let op_msg = voice.Op.asString(op)
		let d = {
			op: op
		}

		if (data !== undefined)
			d.d = data

		d = JSON.stringify(d)

		logger.debug(`voice.Gw.send(op = ${op_msg}): ${d}`)

		this.ws.send(d)
	}

	sendIdent()
	{
		let payload = {
			server_id: this.idServer,
			user_id: this.idUser,
			session_id: this.idSession,
			token: this.token
		}

		return this.send(voice.Op.IDENT, payload)
	}

	sendSelectProto()
	{
		let media_desc = this.localSdp.mediaDescs[0]

		const session_attrs_keep = [
			sdp.AttributeFieldField.EXTMAP,
			sdp.AttributeFieldField.EXTMAP_ALLOW_MIXED,
			sdp.AttributeFieldField.ICE_LITE,
			sdp.AttributeFieldField.ICE_OPTIONS,
			sdp.AttributeFieldField.ICE_PACING,
			sdp.AttributeFieldField.ICE_PWD,
			sdp.AttributeFieldField.ICE_UFRAG
		]
		let attrs_session = this.localSdp.attrs.filter(a => session_attrs_keep.includes(a.field))

		const media_attrs_keep = [
			sdp.AttributeFieldField.EXTMAP,
			sdp.AttributeFieldField.EXTMAP_ALLOW_MIXED,
			sdp.AttributeFieldField.ICE_MISMATCH,
			sdp.AttributeFieldField.ICE_PWD,
			sdp.AttributeFieldField.ICE_UFRAG
		]
		let attrs_media = media_desc.attrs.filter(a => media_attrs_keep.includes(a.field))

		attrs_session = attrs_session.filter(sa => !attrs_media.some(ma => ma.field === sa.field))
		let rtpmaps = media_desc.attrs.filter(a => a.field === sdp.AttributeFieldField.RTPMAP
				&& media_desc.media.fmts.some(fmt => fmt === a.value.payloadType))

		let attrs = attrs_session.concat(attrs_media, rtpmaps)
		let sdp_ = attrs.map(a => a.toSdp()).join('\n') + '\n'

		let codecs = rtpmaps.map(a => ({
			name: a.value.encodingName,
			type: media_desc.media.media,
			priority: a.value.encodingName === sdp.RtpPayloadFormatMediaType.OPUS ? 2000 : 1000,
			payload_type: a.value.payloadType | 0
		}))

		return this.send(voice.Op.SELECT_PROTO, {
			protocol: 'webrtc',
			data: sdp_,
			sdp: sdp_,
			codecs: codecs,
			rtc_connection_id: util.uuidToStr(util.genUuidv4())
		})
	}

	sendHeartbeat()
	{
		return this.send(voice.Op.HEARTBEAT, Date.now() | 0)
	}

	sendSpeaking(speaking)
	{
		speaking ??= voice.Speaking.NONE

		let local_media = this.localSdp.mediaDescs[0]
		let ssrc = local_media.attrs.filter(a => a.field === sdp.AttributeFieldField.SSRC)[0].value.split(' ')[0]

		return this.send(voice.Op.SPEAKING, {
			speaking: speaking,
			delay: 0,
			ssrc: ssrc
		})
	}

	onWsMsg(ev)
	{
		super.onWsMsg(ev)

		let data

		try
		{
			data = JSON.parse(ev.data)
		}
		catch (ex)
		{
			throw new Error(`Invalid message: ${ev.data}`)
		}

		let op_msg = voice.Op.asString(data.op)

		logger.debug(`voice.Gw.onWsMsg(): op = ${op_msg}`)

		switch (data.op)
		{
			case voice.Op.READY: this.onMsgReady(data); break
			case voice.Op.SESSION_DESCR: this.onMsgSessionDescr(data); break
			case voice.Op.HEARTBEAT_ACK: this.onMsgHeartbeatAck(data); break
			case voice.Op.HELLO: this.onMsgHello(data); break
			default:
				logger.debug(`voice.Gw.onWsMsg(): Message not handled`)
				break
		}
	}

	onWsClose(ev)
	{
		if (ev.code === voice.CloseEventCode.INVAL_SESSION
				|| ev.code === voice.CloseEventCode.DISCONNECTED)
		{
			// Disable auto-reconnect mechanism
			this.autoReconnectTimeout = -1
		}

		super.onWsClose(ev)

		let close_msg = voice.CloseEventCode.asString(ev.code)

		let reason = ev.reason
		if (reason !== '')
			reason = `: ${reason}`

		logger.debug(`voice.Gw.onWsClose(): ${close_msg}${reason}`)

		window.clearInterval(this.heartbeatIntervalId)

		if (this.rpc !== null)
			this.rpc.close()
		this.rpc = null

		if (ev.code === voice.CloseEventCode.INVAL_SESSION)
		{
			// Custom auto reconnect: TODO
		}
	}

	async onMsgReady(data)
	{
		let d = data.d

		logger.debug('voice.Gw.onMsgReady()')

		this.rpc = new RTCPeerConnection()

		logger.debug('voice.Gw.onMsgReady(): Created new RTCPeerConnection')

		this.rpc.onsignalingstatechange = ev => {
			logger.debug('rpc.signalingState: ', this.rpc.signalingState)
		}

		this.rpc.onconnectionstatechange = ev => {
			logger.debug('rpc.connectionState: ', this.rpc.connectionState)
		}

		this.rpc.oniceconnectionstatechange = ev => {
			logger.debug('rpc.iceConnectionState: ', this.rpc.iceConnectionState)

			if (this.rpc.iceConnectionState === 'connected')
			{
				if (this.direction & voice.Direction.IN)
				{
					let strm = new MediaStream(this.rpc.getReceivers().map(receiver => receiver.track))

					this.audioInMediaStream = this.audioCtx.createMediaStreamSource(strm)
					this.audioInMediaStream.mediaStream.getTracks().forEach(track => this.rpc.addTrack(track, this.audioInMediaStream.mediaStream))
				}

				if (this.direction & voice.Direction.OUT)
					this.sendSpeaking(voice.Speaking.MICROPHONE)
			}
		}

		this.rpc.onicegatheringstatechange = ev => {
			logger.debug('rpc.iceGatheringState: ', this.rpc.iceGatheringState)
		}

		this.rpc.onicecandidate = ev => {
			logger.debug('rpc.onicecandidate: ', ev.candidate)
		}

		if (this.direction & (voice.Direction.IN | voice.Direction.OUT))
			this.audioCtx = new AudioContext()

		if (this.direction & voice.Direction.OUT)
		{
			this.audioOutMediaStream = this.audioCtx.createMediaStreamDestination()
			this.audioOutMediaStream.stream.getTracks().forEach(track => this.rpc.addTrack(track, this.audioOutMediaStream.stream))
		}

		let offer = await this.rpc.createOffer()
		await this.rpc.setLocalDescription(offer)

		this.localSdp = new sdp.Sdp(this.rpc.localDescription.sdp)
		if (this.localSdp.mediaDescs.length !== 1)
			throw new Error(`Unexpected local media desc count ${this.localSdp.mediaDescs.length} (expected 1)`)

		this.sendSelectProto()
	}

	async onMsgSessionDescr(data)
	{
		let d = data.d
		/* There is only one local track, so only one local media desc */
		let local_media = this.localSdp.mediaDescs[0]
		/* Select the local rtpmap matching the requested remote audio codec */
		let local_rtpmap = local_media.attrs.filter(a => a.field === sdp.AttributeFieldField.RTPMAP
				&& local_media.media.fmts.some(fmt => fmt === a.value.payloadType)
				&& a.value.encodingName === d.audio_codec)

		if (local_rtpmap.length < 1)
			throw new Error(`No local rtpmap found for remote codec '${d.audio_codec}'`)
		local_rtpmap = local_rtpmap[0]

		/* Fixes because Discord is dumb */
		let remote_sdp_fixed = d.sdp
				.split('\n')
				.sort((a, b) => {
					const map = {
						m: 0,
						i: 1,
						c: 2,
						b: 3,
						k: 4,
						a: 5
					}

					return map[a[0]] - map[b[0]]
				})
				.map(e => {
					if (e[0] === 'm')
						return `${e} 0`
					return e
				})
				.join('\r\n')
		let remote_media = new sdp.MediaDesc(remote_sdp_fixed)
		let mid = '0'

		let sdp_ = new sdp.Sdp()
		let attr

		attr = new sdp.AttributeField()
		attr.field = sdp.AttributeFieldField.GROUP
		attr.value = new sdp.AttributeGroupValue()
		attr.value.semantics = 'BUNDLE'
		attr.value.idTags = [ mid ]
		sdp_.attrs.push(attr)

		attr = new sdp.AttributeField()
		attr.field = sdp.AttributeFieldField.SENDRECV
		sdp_.attrs.push(attr)

		let media = new sdp.MediaDesc()
		media.conData = remote_media.conData
		media.bandwidths = remote_media.bandwidths
		media.key = remote_media.key

		let m = new sdp.MediaField()
		m.media = sdp.MediaType.AUDIO
		m.port = remote_media.media.port
		m.range = remote_media.media.range
		m.proto = sdp.MediaProtoType.UDP_TLS_RTP_SAVPF
		m.fmts = [ local_rtpmap.value.payloadType ]
		media.media = m

		const media_attrs_keep = [
			sdp.AttributeFieldField.CANDIDATE,
			sdp.AttributeFieldField.FINGERPRINT,
			sdp.AttributeFieldField.ICE_PWD,
			sdp.AttributeFieldField.ICE_UFRAG,
			sdp.AttributeFieldField.RTCP
		]
		media.attrs = remote_media.attrs.filter(a => media_attrs_keep.includes(a.field))

		const media_extmaps_keep = [
			'urn:ietf:params:rtp-hdrext:ssrc-audio-level'
		]
		media.attrs = media.attrs.concat(local_media.attrs.filter(a => a.field === sdp.AttributeFieldField.EXTMAP
				&& media_extmaps_keep.includes(a.value.split(' ')[1])))

		if (local_rtpmap.value.encodingName === sdp.RtpPayloadFormatMediaType.OPUS)
		{
			attr = new sdp.AttributeField()
			attr.field = sdp.AttributeFieldField.FMTP
			attr.value = `${local_rtpmap.value.payloadType} minptime=10;useinbandfec=1;usedtx=1`
			media.attrs.push(attr)
		}

		attr = new sdp.AttributeField()
		attr.field = sdp.AttributeFieldField.MID
		attr.value = new sdp.AttributeMIDValue()
		attr.value.idTag = mid
		media.attrs.push(attr)

		attr = new sdp.AttributeField()
		attr.field = sdp.AttributeFieldField.RTCP_MUX
		media.attrs.push(attr)

		attr = new sdp.AttributeField()
		attr.field = sdp.AttributeFieldField.RTPMAP
		attr.value = new sdp.AttributeRTPMapValue()
		attr.value.payloadType = local_rtpmap.value.payloadType
		attr.value.encodingName = local_rtpmap.value.encodingName
		attr.value.clockRate = local_rtpmap.value.clockRate
		attr.value.encodingParam = local_rtpmap.value.encodingParam
		media.attrs.push(attr)

		attr = new sdp.AttributeField()

		if ((this.direction & voice.Direction.IN)
				&& (this.direction & voice.Direction.OUT))
			attr.field = sdp.AttributeFieldField.SENDRECV
		else if (this.direction & voice.Direction.IN)
			attr.field = sdp.AttributeFieldField.SENDONLY
		else if (this.direction & voice.Direction.OUT)
			attr.field = sdp.AttributeFieldField.RECVONLY
		else
			attr.field = sdp.AttributeFieldField.INACTIVE

		media.attrs.push(attr)

		attr = new sdp.AttributeField()
		attr.field = sdp.AttributeFieldField.SETUP
		attr.value = 'passive'
		media.attrs.push(attr)

		sdp_.mediaDescs.push(media)

		await this.rpc.setRemoteDescription({
			type: 'answer',
			sdp: sdp_.toSdp()
		})

		this.remoteSdp = new sdp.Sdp(this.rpc.remoteDescription.sdp)
	}

	onMsgHeartbeatAck(data)
	{
		logger.debug('voice.Gw.onMsgHeartbeatAck()')
	}

	onMsgHello(data)
	{
		let d = data.d

		logger.debug('voice.Gw.onMsgHello()')
		this.heartbeatInterval = d.heartbeat_interval

		if (this.heartbeatIntervalId !== null)
			window.clearInterval(this.heartbeatIntervalId)

		this.heartbeatIntervalId = window.setInterval(() => {
			this.sendHeartbeat()
		}, this.heartbeatInterval)

		this.sendIdent()
	}
}
xport(Gw)
