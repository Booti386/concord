let types = await use('concord.types')

class Bitfield
{
	static asString(val)
	{
		let r = ''
		let v = val

		for (let k in this)
		{
			let flag = this[k]

			if (flag & v
					|| (val === 0 && flag === 0))
			{
				if (r !== '')
					r += ' | '
				r += `${k} (${flag})`

				v &= ~flag
			}
		}

		if (v != 0 || r === '')
		{
			if (r !== '')
				r += ' | '
			r += `<unknown> (${v})`
		}

		return r
	}
}
