#!/bin/bash

set -e

DIR="$(readlink -e "$(dirname "${BASH_SOURCE[0]}")")"

function log()
{
	echo "$@" >&2
}

out=out/concord.bundle.js

log "Building '${out}'..."

"${DIR}/js-bundle/bundle.sh" "${DIR}/src" >"${DIR}/${out}"

log "Built '${out}' succesfully."
