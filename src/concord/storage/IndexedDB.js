let storage = await use('concord.storage')

class RequestAccess
{
	static READ = 'readonly'
	static WRITE = 'readwrite'
}

class IndexedDB
{
	name
	db

	constructor(name)
	{
		this.name = name
		this.db = new Promise((resolve, reject) => {
			let rq = window.indexedDB.open(this.name, 1)
			rq.onerror = ev => {
				reject(ev)
			}

			rq.onsuccess = ev => {
				let db = rq.result
				resolve(db)
			}

			rq.onupgradeneeded = ev => {
				let db = rq.result
				db.createObjectStore('root', { autoIncrement: true })
				return true
			}
		})
	}

	async _emitRequest(rq_name, access, ...args)
	{
		let db = await this.db

		let transaction = db.transaction('root', access)
		let store = transaction.objectStore('root')
		let rq = store[rq_name].apply(store, args)

		return new Promise((resolve, reject) => {
			transaction.onerror = ev => {
				reject(ev)
			}

			rq.onerror = ev => {
				reject(ev)
			}

			rq.onsuccess = ev => {
				resolve(rq.result ?? null)
			}
		})
	}

	async getItem(key)
	{
		let cursor = await this._emitRequest('openCursor', RequestAccess.READ, key)
		if (cursor === null)
			throw new Error(`No value for key '${key}'`)

		return cursor.value
	}

	async getItemOrElse(key, or_else)
	{
		return this.getItem(key).catch(() => or_else)
	}

	async setItem(key, value)
	{
		return this._emitRequest('put', RequestAccess.WRITE, value, key)
	}

	async removeItem(key)
	{
		return this._emitRequest('delete', RequestAccess.WRITE, key)
	}

	async clear()
	{
		return this._emitRequest('clear', RequestAccess.WRITE)
	}

	async getItems(keys)
	{
		return Promise.all(keys.map(key => this.getItem(key)))
	}

	async getItemsOrElse(keys, or_else)
	{
		return Promise.all(keys.map((key, idx) => this.getItemOrElse(key, or_else[idx])))
	}

	async setItems(key_vals)
	{
		return Promise.all(Object.entries(key_vals).map(([key, val]) => this.setItem(key, val)))
	}
}
