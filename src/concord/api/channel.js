let Bitfield = await use('concord.types.Bitfield')
let Enum = await use('concord.types.Enum')

let channel = await ready('concord.api.channel')

class Type extends Enum
{
	static GUILD_TEXT = 0
	static DM = 1
	static GUILD_VOICE = 2
	static GROUP_DM = 3
	static GUILD_CATEGORY = 4
	static GUILD_NEWS = 5
	static GUILD_STORE = 6
}
xport(Type)
