let types = await use('concord.types')

class Enum
{
	static asString(val)
	{
		for (let k in this)
		{
			if (this[k] === val)
				return `${k} (${val})`
		}

		return `<unknown> (${val})`
	}
}
