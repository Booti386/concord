let Enum = await use('concord.types.Enum')

let state = await ready('concord.state')

class Type extends Enum
{
	// Virtual type, indicates end of array or object
	static DELIMITER = ';'
	static ARRAY = 'a'
	static NUMBER = 'f'
	static BIG_INT = 'i'
	static NULL = 'n'
	static OBJECT = 'o'
	static TRUE = 'r'
	static STRING = 's'
	static UNDEFINED = 'u'
	static FALSE = 'w'
	static STATE_OBJECT = 'z'

	static typeOf(v)
	{
		const tmap = {
			bigint: this.BIG_INT,
			false: this.FALSE,
			function: undefined, // unserializable
			null: this.NULL,
			number: this.NUMBER,
			object: this.OBJECT,
			string: this.STRING,
			true: this.TRUE,
			undefined: this.UNDEFINED
		}
		let t = typeof v

		if (v === null)
			t = 'null'
		else if (v === true)
			t = 'true'
		else if (v === false)
			t = 'false'

		t = tmap[t] ?? null
		if (t === null)
			return null

		if (t === this.OBJECT)
		{
			if (v instanceof State)
				t = this.STATE_OBJECT
			else if (Array.isArray(v))
				t = this.ARRAY
		}

		return t
	}
}
xport(Type)

class State
{
	static classes = {}

	static register(cls)
	{
		if (!(cls.prototype instanceof State))
			throw new Error(`Class '${cls.name}' does not extend State`)

		this.classes[cls.name] = cls
	}

	static serialize(v)
	{
		let t = Type.typeOf(v)

		switch (t)
		{
			case Type.TRUE:
			case Type.FALSE:
			case Type.NULL:
			case Type.UNDEFINED:
				v = ''
				break

			case Type.BIG_INT:
			case Type.NUMBER:
				v = `${v}${Type.DELIMITER}`
				break

			case Type.STRING:
				t += `${v.length}${Type.DELIMITER}`
				break

			case Type.STATE_OBJECT:
				t += this.serialize(v.constructor.name).substring(1)
			case Type.OBJECT:
			case Type.ARRAY:
			{
				let nv = ''

				for (let i in v)
				{
					let k = this.serialize(i).substring(1)
					let e = this.serialize(v[i])

					nv += `${k}${e}`
				}

				v = `${nv}${Type.DELIMITER}`
				break
			}

			case null:
				throw new Error(`Cannot serialize an expression of type '${typeof v}'`)
		}

		return `${t}${v}`
	}

	static _unserializeHelper(ctx)
	{
		let t = ctx.v[0]
		ctx.v = ctx.v.substring(1)

		let class_n = null
		let obj = null

		switch (t)
		{
			case Type.TRUE: return true
			case Type.FALSE: return false
			case Type.NULL: return null
			case Type.UNDEFINED: return undefined

			case Type.BIG_INT:
				class_n ??= BigInt
			case Type.NUMBER:
			{
				class_n ??= Number

				let m = ctx.v.match(/^([^;]+);/)
				if (m === null)
					break;

				let w = m[0]
				let n = m[1]

				ctx.v = ctx.v.substring(w.length)
				return class_n(n)
			}

			case Type.STRING:
			{
				let m = ctx.v.match(/^(\d+);/)
				if (m === null)
					break;

				let w = m[0]
				let n = m[1] | 0
				let s = ctx.v.substring(w.length, w.length + n)

				ctx.v = ctx.v.substring(w.length + n)
				return s
			}

			case Type.STATE_OBJECT:
				if (obj === null)
				{
					ctx.v = `${Type.STRING}${ctx.v}`
					obj = new (this.classes[this._unserializeHelper(ctx)])()
				}
			case Type.OBJECT:
				obj ??= {}
			case Type.ARRAY:
			{
				obj ??= []

				while (ctx.v[0] !== Type.DELIMITER)
				{
					ctx.v = `${Type.STRING}${ctx.v}`
					let k = this._unserializeHelper(ctx)
					let e = this._unserializeHelper(ctx)

					obj[k] = e
				}

				if (ctx.v[0] !== Type.DELIMITER)
					break

				ctx.v = ctx.v.substring(1)
				return obj
			}

			default:
				throw new Error(`Unexpected data type '${t}' encountered while unserializing`)
		}

		throw new Error(`Unexpected data encountered while unserializing value of type '${t}'`)
	}

	static unserialize(v)
	{
		let ctx = {v: v}

		v = this._unserializeHelper(ctx)
		if (ctx.v !== '')
			throw new Error('Unexpected data at the end of serialized value')

		return v
	}

	serialize()
	{
		return this.constructor.serialize(this)
	}

	clone()
	{
		return this.constructor.unserialize(this.serialize())
	}
}
xport(State)
