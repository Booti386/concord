let util = await ready('concord.util')

let staticTextDecoder = new TextDecoder()
let staticTextEncoder = new TextEncoder()

function byteToHex(b)
{
	return (b + 0x100).toString(16).substr(1).toLowerCase()
}
xport(byteToHex)

function stringByteLength(s)
{
	return staticTextEncoder.encode(s).length
}
xport(stringByteLength)

function str2bin(s)
{
	if (s.length === 0)
		return ''
	return staticTextEncoder.encode(s)
			.reduce((acc, c) => `${acc}${String.fromCharCode(c)}`, '')
}
xport(str2bin)

function bin2str(s)
{
	if (s.length === 0)
		return ''
	return staticTextDecoder.decode(Uint8Array.from(s, c => c.charCodeAt(0)))
}
xport(bin2str)

function binStrTrunc(s, max_len, abbrev)
{
	abbrev ??= ''

	if (max_len >= s.length)
		return s

	if (max_len === 0)
		return ''

	if (max_len >= abbrev.length)
		max_len -= abbrev.length
	else
		abbrev = ''

	let len = max_len
	while (len > 0 && (s.charCodeAt(len - 1) & 0xC0) === 0x80)
		len--

	if (len > 0 && s.charCodeAt(len - 1) >= 0x80)
		len--

	return `${s.substring(0, len)}${abbrev}`
}
xport(binStrTrunc)

function genSessId()
{
	return Math.random().toString().substr(2, 21)
}
xport(genSessId)

function genRandBytes(count)
{
	let buf = new Uint8Array(count)

	window.crypto.getRandomValues(buf)

	return buf
}
xport(genRandBytes)

function genUuidv4()
{
	let buf = util.genRandBytes(16)

	buf[6] = (buf[6] & 0x0f) | 0x40
	buf[8] = (buf[8] & 0x3f) | 0x80

	return buf
}
xport(genUuidv4)

function uuidToStr(uuid)
{
	let str = ''

	for (let i in uuid)
	{
		let b = uuid[i]

		str += util.byteToHex(b)

		if (i == 3 || i == 5 || i == 7 || i == 9)
			str += '-'
	}

	return str
}
xport(uuidToStr)

function countDailySplitsBetween(low, high, splits)
{
	let count = 0
	let low_up = (new Date(low)).setHours(24, 0, 0, 0)
	let high_down = (new Date(high)).setHours(0, 0, 0, 0)

	for (let s of splits)
	{
		let tmpl = (new Date(low)).setHours(...s)
		let tmph = (new Date(high)).setHours(...s)

		// Both are on the same day
		if (low_up > high_down)
		{
			if (tmpl >= low && tmph <= high)
				count++
			continue
		}

		if (tmpl >= low)
			count++
		if (tmph <= high)
			count++
	}

	if (low_up < high_down)
		count += splits.length * ((high_down - low_up) / (1000 * 3600 * 24) | 0)

	return count
}
xport(countDailySplitsBetween)

class PollPromise extends Promise
{
	_resolve
	_reject
	_timerId
	cb
	timeout

	constructor(cb, timeout, _no_wrap)
	{
		_no_wrap ??= false
		if (!_no_wrap)
			return super(cb)

		let res = null
		let rej = null

		super((resolve, reject) => {
			res = resolve
			rej = reject
		})

		this._resolve = res
		this._reject = rej
		this._timerId = null
		this.cb = cb
		this.timeout = timeout

		let cb_wrap = () => {
			try
			{
				let result = this.cb()
				if (result ?? null !== null)
				{
					this._timerId = null
					return this._resolve(result)
				}
			}
			catch (e)
			{
				this._timerId = null
				return this._reject(e)
			}

			this._timerId = window.setTimeout(() => cb_wrap(), this.timeout)
		}

		cb_wrap()
	}

	cancel(reason)
	{
		if (this._timerId === null)
			return

		window.clearInterval(this._timerId)
		this._timerId = null

		this._reject(reason)
	}

	static create(cb, timeout)
	{
		return new PollPromise(cb, timeout, true)
	}
}

function poll(cb, timeout)
{
	return PollPromise.create(cb, timeout)
}
xport(poll)
