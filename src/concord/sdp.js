let logger = await use('concord.logger')

let sdp = await ready('concord.sdp')

class BNF
{
	/* ABNF */

	// SP = " "
	// ALPHA = "A"-"Z" / "a"-"z"
	// DIGIT = "0"-"9"
	// HEXDIG = DIGIT / "A" / "B" / "C" / "D" / "E" / "F"
	// VCHAR = %x21-7E
	static VCHAR = '(?:[^\\x00-\\x20\\x7F])'

	/* SDP */

	// Base

	// POS-DIGIT = "1"-"9"
	// alpha-numeric = ALPHA / DIGIT
	// token-char = %x21 / %x23-27 / %x2A-2B / %x2D-2E / %x30-39 / %x41-5A / %x5E-7E
	static TOKEN_CHAR = '[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2E\\x30-\\x39\\x41-\\x5A\\x5E-\\x7E]'
	// byte-string = 1*(%x01-09/%x0B-0C/%x0E-FF)
	static BYTE_STRING = '(?:[^\\x00\\x0A\\x0D]+)'
	// integer = POS-DIGIT *DIGIT
	static INTEGER = '(?:[1-9]\\d*)'
	// zero-based-integer = "0" / integer
	static ZERO_BASED_INTEGER = `(?:0|${this.INTEGER})`
	// decimal-uchar = DIGIT
	//                 / POS-DIGIT DIGIT
	//                 / ("1" 2*(DIGIT))
	//                 / ("2" ("0"/"1"/"2"/"3"/"4") DIGIT)
	//                 / ("2" "5" ("0"/"1"/"2"/"3"/"4"/"5"))
	static DECIMAL_UCHAR = '(?:\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])'
	// b1 = decimal-uchar ; less than "224"
	static B1 = '(?:\\d|[1-9]\\d|1\\d\\d|2[0-1]\\d|22[0-4])'
	// m1 = ("22" ("4"/"5"/"6"/"7"/"8"/"9")) / ("23" DIGIT )
	static M1 = '(?:22[4-9]|23\\d)'
	// hex4 = 1*4HEXDIG
	static HEX4 = '(?:[\\dA-F]{1,4})'
	// hexseq = hex4 *( ":" hex4)
	static HEXSEQ = `(?:${this.HEX4}(?::${this.HEX4})*)`
	// hexpart = hexseq / hexseq "::" [ hexseq ] / "::" [ hexseq ]
	static HEXPART = `(?:${this.HEXSEQ}|${this.HEXSEQ}?::${this.HEXSEQ}?)`
	// base64-char = ALPHA / DIGIT / "+" / "/"
	static BASE64_CHAR = '[A-Za-z\\d+/]'
	// base64-unit = 4base64-char
	static BASE64_UNIT = `(?:${this.BASE64_CHAR}{4})`
	// base64-pad = 2base64-char "==" / 3base64-char "="
	static BASE64_PAD = `(?:${this.BASE64_CHAR}{2}==|${this.BASE64_CHAR}{3}=)`

	// text = byte-string
	static TEXT = this.BYTE_STRING
	// non-ws-string = 1*(VCHAR/%x80-FF)
	static NON_WS_STRING = `(?:${this.VCHAR}+)`
	// token = 1*(token-char)
	static TOKEN = `(?:${this.TOKEN_CHAR}+)`
	// ttl = (POS-DIGIT *2DIGIT) / "0"
	static TTL = '(?:[1-9]\\d{0,2}|0)'
	// IP4-address = b1 3("." decimal-uchar)
	static IP4_ADDRESS = `(?:${this.B1}(?:\\.${this.DECIMAL_UCHAR}){3})`
	// IP4-multicast = m1 3( "." decimal-uchar )
    //                 "/" ttl [ "/" integer ]
	static IP4_MULTICAST = `(?:${this.M1}(?:\\.${this.DECIMAL_UCHAR}){3}/${this.TTL}(?:/${this.INTEGER})?)`
	// IP6-address = hexpart [ ":" IP4-address ]
	static IP6_ADDRESS = `(?:${this.HEXPART}(?::${this.IP4_ADDRESS})?)`
	// IP6-multicast = hexpart [ "/" integer ]
	static IP6_MULTICAST = `(?:${this.HEXPART}(?:/${this.INTEGER})?)`
	// FQDN = 4*(alpha-numeric / "-" / ".")
	static FQDN = `(?:[A-Za-z\\d.-]{4,})`
	// extn-addr = non-ws-string
	static EXTN_ADDR = this.NON_WS_STRING
	// unicast-address = IP4-address / IP6-address / FQDN / extn-addr
	static UNICAST_ADDRESS = `(?:${this.IP4_ADDRESS}|${this.IP6_ADDRESS}|${this.FQDN}|${this.EXTN_ADDR})`
	// multicast-address = IP4-multicast / IP6-multicast / FQDN / extn-addr
	static MULTICAST_ADDRESS = `(?:${this.IP4_MULTICAST}|${this.IP6_MULTICAST}|${this.FQDN}|${this.EXTN_ADDR})`
	// uri = URI-reference
	static URI = '(?:.+)' // TODO
	// email-address = address-and-comment / dispname-and-address / addr-spec
	static EMAIL_ADDRESS = '(?:.+)' // TODO
	// phone-number = phone *SP "(" 1*email-safe ")" /
    //                1*email-safe "<" phone ">" /
    //                phone
	static PHONE_NUMBER = '(?:.+)' // TODO
	// time = POS-DIGIT 9*DIGIT
	static TIME = `(?:[1-9]\\d{9,})`
	// base64 = *base64-unit [base64-pad]
	static BASE64 = `(?:${this.BASE64_UNIT}*${this.BASE64_PAD}?)`
	// media = token
	static MEDIA = this.TOKEN

	// Special

	// username = non-ws-string
	static USERNAME = this.NON_WS_STRING
	// sess-id = 1*DIGIT
	static SESS_ID = '(?:\\d+)'
	// sess-version = 1*DIGIT
	static SESS_VERSION = '(?:\\d+)'
	// nettype = token
	static NETTYPE = this.TOKEN
	// addrtype = token
	static ADDRTYPE = this.TOKEN
	// connection-address =  multicast-address / unicast-address
	static CONNECTION_ADDRESS = `(?:${this.MULTICAST_ADDRESS}|${this.UNICAST_ADDRESS})`
	// bwtype = token
	static BWTYPE = this.TOKEN
	// bandwidth = 1*DIGIT
	static BANDWIDTH = '(?:\\d+)'
	// start-time = time / "0"
	static START_TIME = `(?:${this.TIME}|0)`
	// stop-time = time / "0"
	static STOP_TIME = `(?:${this.TIME}|0)`
	// repeat-interval = POS-DIGIT *DIGIT [fixed-len-time-unit]
	static REPEAT_INTERVAL = '(?:[1-9]\\d*[dhms]?)'
	// typed-time = 1*DIGIT [fixed-len-time-unit]
	static TYPED_TIME = '(?:\\d+[dhms]?)'
	// key-type = %x70 %x72 %x6f %x6d %x70 %x74 /     ; "prompt"
    //            %x63 %x6c %x65 %x61 %x72 ":" text / ; "clear:"
    //            %x62 %x61 %x73 %x65 "64:" base64 /  ; "base64:"
    //            %x75 %x72 %x69 ":" uri              ; "uri:"
	static KEY_TYPE = `(?:prompt|clear:${this.TEXT}|base64:${this.BASE64}|uri:${this.URI})`
	// att-field = token
	static ATT_FIELD = this.TOKEN
	// att-value = byte-string
	static ATT_VALUE = this.BYTE_STRING
	// port = 1*DIGIT
	static PORT = `(?:\\d+)`
	// proto = token *("/" token)
	static PROTO = `(?:${this.TOKEN}(?:/${this.TOKEN})*)`
	// fmt = token
	static FMT = this.TOKEN

	// Whole-line

	// session-description = proto-version
	//                       origin-field
	//                       session-name-field
	//                       information-field
	//                       uri-field
	//                       email-fields
	//                       phone-fields
	//                       connection-field
	//                       bandwidth-fields
	//                       time-fields
	//                       key-field
	//                       attribute-fields
	//                       media-descriptions

	// proto-version = %x76 "=" 1*DIGIT CRLF
	static PROTO_VERSION_W = '^v=(\\d+)$'
	// origin-field = %x6f "=" username SP sess-id SP sess-version SP nettype SP addrtype SP unicast-address CRLF
	static ORIGIN_FIELD_W = `^o=(${this.USERNAME}) (${this.SESS_ID}) (${this.SESS_VERSION}) (${this.NETTYPE}) (${this.ADDRTYPE}) (${this.UNICAST_ADDRESS})$`
	// session-name-field = %x73 "=" text CRLF
	static SESSION_NAME_FIELD_W = `^s=(${this.TEXT})$`
	// information-field = [%x69 "=" text CRLF]
	static INFORMATION_FIELD_W = `^i=(${this.TEXT})$`
	// uri-field = [%x75 "=" uri CRLF]
	static URI_FIELD_W = `^u=(${this.URI})$`
	// email-field = [%x65 "=" email-address CRLF]
	static EMAIL_FIELD_W = `^e=(${this.EMAIL_ADDRESS})$`
	// phone-field = [%x70 "=" phone-number CRLF]
	static PHONE_FIELD_W = `^p=(${this.PHONE_NUMBER})$`
	// connection-field = [%x63 "=" nettype SP addrtype SP connection-address CRLF]
	static CONNECTION_FIELD_W = `^c=(${this.NETTYPE}) (${this.ADDRTYPE}) (${this.CONNECTION_ADDRESS})$`
	// bandwidth-field = *(%x62 "=" bwtype ":" bandwidth CRLF)
	static BANDWIDTH_FIELD_W = `^b=(${this.BWTYPE}):(${this.BANDWIDTH})$`
	// time-fields = 1*( %x74 "=" start-time SP stop-time
    //               *(CRLF repeat-fields) CRLF)
	//               [zone-adjustments CRLF]
	// time-field = %x74 "=" start-time SP stop-time CRLF
	static TIME_FIELD_W = `^t=(${this.START_TIME}) (${this.STOP_TIME})$`
	// repeat-fields = %x72 "=" repeat-interval SP typed-time 1*(SP typed-time)
	static REPEAT_FIELD_W = `^r=(${this.REPEAT_INTERVAL}) (${this.TYPED_TIME}) (${this.TYPED_TIME}(?: ${this.TYPED_TIME})*)$`
	// zone-adjustments = %x7a "=" time SP ["-"] typed-time *(SP time SP ["-"] typed-time)
	static ZONE_ADJUSTMENTS_W = `^z=(${this.TIME} -?${this.TYPED_TIME}(?: ${this.TIME} -?${this.TYPED_TIME})*)$`
	// key-field = [%x6b "=" key-type CRLF]
	static KEY_FIELD_W = `^k=(${this.KEY_TYPE})$`
	// attribute-field = [%x61 "=" attribute CRLF]
	// attribute = (att-field ":" att-value) / att-field
	static ATTRIBUTE_FIELD_W = `^a=(${this.ATT_FIELD})(?::(${this.ATT_VALUE}))?$`

	// media-descriptions =  *( media-field
	//                       information-field
	//                       *connection-field
	//                       bandwidth-fields
	//                       key-field
	//                       attribute-fields )

	// media-field = %x6d "=" media SP port ["/" integer] SP proto 1*(SP fmt) CRLF
	static MEDIA_FIELD_W = `^m=(${this.MEDIA}) (${this.PORT})(?:/(${this.INTEGER}))? (${this.PROTO}) (${this.FMT}(?: ${this.FMT})*)$`

	// Attribute values

	// RFC4566

	// rtpmap-value = payload-type SP encoding-name
	//   "/" clock-rate [ "/" encoding-params ]
	// payload-type = zero-based-integer
	// encoding-name = token
	// clock-rate = integer
	// encoding-params = channels
	// channels = integer
	static ATTRIBUTE_RTPMAP_VALUE_W = `^(${this.ZERO_BASED_INTEGER}) (${this.TOKEN})/(${this.INTEGER})(?:/(${this.INTEGER}))?$`

	// RFC5888

	// mid-attribute      = "a=mid:" identification-tag
	// identification-tag = token
	static ATTRIBUTE_MID_VALUE_W = `^(${this.TOKEN})$`

	// group-attribute     = "a=group:" semantics
	//                       *(SP identification-tag)
	// semantics           = "LS" / "FID" / semantics-extension
	// semantics-extension = token
	static ATTRIBUTE_GROUP_VALUE_W = `^(LS|FID|${this.TOKEN})(?:| (${this.TOKEN}(?: ${this.TOKEN})*))$`

	static init()
	{
		for (let v in this)
		{
			if (typeof v !== 'string')
				continue

			this[v] = new RegExp(this[v])
		}
	}
}
xport(BNF)

sdp.BNF.init()

class Sdp
{
	protoVersion = '0'
	origin = new sdp.OriginField()
	sessName = ' '
	info = null
	uri = null
	email = null
	phone = null
	conData = null
	bandwidths = []
	timeDescs = [ new sdp.TimeDesc() ]
	timeZones = []
	key = null
	attrs = []
	mediaDescs = []

	constructor(data)
	{
		if (data === undefined
				|| data === null)
			return

		this.from(data)
	}

	from(data)
	{
		if (typeof data === 'string')
			data = data.split('\r\n')

		let l = data.shift()
		if (l === undefined
				|| l[0] !== 'v')
			throw new Error('Missing required \'v=\' field')

		let f = l.match(sdp.BNF.PROTO_VERSION_W)
		if (f === null)
			throw new Error('Invalid \'v=\' field')
		this.protoVersion = f[1]

		l = data.shift()
		if (l === undefined
				|| l[0] !== 'o')
			throw new Error('Missing required \'o=\' field')

		this.origin = new sdp.OriginField(l)

		l = data.shift()
		if (l === undefined
				|| l[0] !== 's')
			throw new Error('Missing required \'s=\' field')

		f = l.match(sdp.BNF.SESSION_NAME_FIELD_W)
		if (f === null)
			throw new Error('Invalid \'s=\' field')
		this.sessName = f[1]

		l = data.shift()
		if (l !== undefined
				&& l[0] === 'i')
		{
			f = l.match(sdp.BNF.INFORMATION_FIELD_W)
			if (f === null)
				throw new Error('Invalid \'i=\' field')
			this.info = f[1]

			l = data.shift()
		}

		if (l !== undefined
				&& l[0] === 'u')
		{
			f = l.match(sdp.BNF.URI_FIELD_W)
			if (f === null)
				throw new Error('Invalid \'u=\' field')
			this.uri = f[1]

			l = data.shift()
		}

		if (l !== undefined
				&& l[0] === 'e')
		{
			f = l.match(sdp.BNF.EMAIL_FIELD_W)
			if (f === null)
				throw new Error('Invalid \'e=\' field')
			this.email = f[1]

			l = data.shift()
		}

		if (l !== undefined
				&& l[0] === 'p')
		{
			f = l.match(sdp.BNF.PHONE_FIELD_W)
			if (f === null)
				throw new Error('Invalid \'p=\' field')
			this.phone = f[1]

			l = data.shift()
		}

		if (l !== undefined
				&& l[0] === 'c')
		{
			this.conData = new sdp.ConnectionField(l)

			l = data.shift()
		}

		while (l !== undefined
				&& l[0] === 'b')
		{
			this.bandwidths.push(new sdp.BandwidthField(l))

			l = data.shift()
		}

		if (l === undefined
				|| l[0] !== 't')
			throw new Error('Missing required \'t=\' field')

		this.timeDescs = []
		do
		{
			data.unshift(l)
			this.timeDescs.push(new sdp.TimeDesc(data))

			l = data.shift()
		}
		while (l !== undefined
				&& l[0] === 't')

		while (l !== undefined
				&& l[0] === 'z')
		{
			this.timeZones.push(new sdp.ZoneAdjustments(l))

			l = data.shift()
		}

		if (l !== undefined
				&& l[0] === 'k')
		{
			this.key = new sdp.KeyField(l)

			l = data.shift()
		}

		while (l !== undefined
				&& l[0] === 'a')
		{
			this.attrs.push(new sdp.AttributeField(l))

			l = data.shift()
		}

		while (l !== undefined
				&& l[0] === 'm')
		{
			data.unshift(l)
			this.mediaDescs.push(new sdp.MediaDesc(data))

			l = data.shift()
		}

		if (l === undefined)
			throw new Error('Missing final EOL')

		if (l !== ''
				|| data.length > 1)
			throw new Error('Unexpected data after the end of the SDP: ${l}')

		this.validate()
	}

	validate()
	{
		// Check for missing required props

		if (this.protoVersion === null)
			throw new Error('Required prop \'protoVersion\' not defined')

		if (this.origin === null)
			throw new Error('Required prop \'origin\' not defined')

		if (this.sessName === null)
			throw new Error('Required prop \'sessName\' not defined')

		if (this.timeDescs.length === 0)
			throw new Error('Required prop \'timeDescs\' contains less than 1 entry')

		// Validate props content

		if (this.protoVersion !== '0')
			throw new Error(`Unexpected value '${this.protoVersion}' for prop 'protoVersion' ('0' was expected)`)

		this.origin.validate()

		if (this.sessName.length === 0)
			throw new Error(`Unexpected value '${this.sessName}' for prop 'sessName' (a non-empty string was expected)`)

		// Nothing to do for info

		// TODO: validate uri

		// Ignore email and phone

		// Validate conData only if present
		// mediaDesc.validate() will later ensure that conData is present
		// in all the media desc if it is missing from the session desc
		if (this.conData !== null)
			this.conData.validate()

		for (let bdw of this.bandwidths)
			bdw.validate()

		for (let desc of this.timeDescs)
			desc.validate()

		for (let tz of this.timeZones)
			tz.validate()

		// TODO: Validate key

		// TODO: Validate attributes

		for (let desc of this.mediaDescs)
			desc.validate()
	}

	toSdp()
	{
		return this.toSdpArray().join('\r\n')
	}

	toSdpArray()
	{
		let data = []

		this.validate()

		data.push(`v=${this.protoVersion}`)
		data.push(this.origin.toSdp())
		data.push(`s=${this.sessName}`)

		if (this.info !== null)
			data.push(`i=${this.info}`)

		if (this.uri !== null)
			data.push(`u=${this.uri}`)

		if (this.email !== null)
			data.push(`e=${this.email}`)

		if (this.phone !== null)
			data.push(`p=${this.phone}`)

		if (this.conData !== null)
			data.push(this.conData.toSdp())

		for (let bandwidth of this.bandwidths)
			data.push(bandwidth.toSdp())

		for (let time_desc of this.timeDescs)
			data = data.concat(time_desc.toSdpArray())

		for (let tz of this.timeZones)
			data.push(tz.toSdp())

		if (this.key !== null)
			data.push(this.key.toSdp())

		for (let attr of this.attrs)
			data.push(attr.toSdp())

		for (let media_desc of this.mediaDescs)
			data = data.concat(media_desc.toSdpArray())

		data.push('')

		return data
	}
}
xport(Sdp)

class OriginFieldUsername
{
	static NONE = '-'
}
xport(OriginFieldUsername)

class OriginFieldSessVer
{
	static INITIAL = '0'
}
xport(OriginFieldSessVer)

class NetType
{
	static IN = 'IN'
}
xport(NetType)

class AddrType
{
	static IP4 = 'IP4'
	static IP6 = 'IP6'
}
xport(AddrType)

class Addr
{
	static ANY_IP4 = '0.0.0.0'
	static ANY_IP6 = '::'
}
xport(Addr)

class OriginField
{
	username = sdp.OriginFieldUsername.NONE
	sessId = this.genSessId()
	sessVer = sdp.OriginFieldSessVer.INITIAL
	netType = sdp.NetType.IN
	addrType = sdp.AddrType.IP4
	addr = sdp.Addr.ANY_IP4

	constructor(data)
	{
		data ??= null

		if (data === null)
			return

		this.from(data)
	}

	genSessId()
	{
		let n = Math.abs(0x7852FE07 ^ Date.now() ^ (Math.random() * 0x80000000))

		this.sessId = `${n}`
		return this.sessId
	}

	from(data)
	{
		let f = data.match(sdp.BNF.ORIGIN_FIELD_W)
		if (f === null)
			throw new Error('Invalid \'o=\' field')

		this.username = f[1]
		this.sessId = f[2]
		this.sessVer = f[3]
		this.netType = f[4]
		this.addrType = f[5]
		this.addr = f[6]

		this.validate()
	}

	validate()
	{
		// TODO
	}

	toSdp()
	{
		return `o=${this.username} ${this.sessId} ${this.sessVer} ${this.netType} ${this.addrType} ${this.addr}`
	}
}
xport(OriginField)

class ConnectionField
{
	netType = sdp.NetType.IN
	addrType = sdp.AddrType.IP4
	addr = sdp.Addr.ANY_IP4

	constructor(data)
	{
		if (data === undefined
				|| data === null)
			return

		this.from(data)
	}

	from(data)
	{
		let f = data.match(sdp.BNF.CONNECTION_FIELD_W)
		if (f === null)
			throw new Error('Invalid \'c=\' field')

		this.netType = f[1]
		this.addrType = f[2]
		this.addr = f[3]

		this.validate()
	}

	validate()
	{
		// TODO
	}

	toSdp()
	{
		return `c=${this.netType} ${this.addrType} ${this.addr}`
	}
}
xport(ConnectionField)

class BandwidthFieldType
{
	static CT = 'CT'
	static AS = 'AS'
}
xport(BandwidthFieldType)

class BandwidthField
{
	type = null
	bandwidth = null

	constructor(data)
	{
		if (data === undefined
				|| data === null)
			return

		this.from(data)
	}

	from(data)
	{
		let f = data.match(sdp.BNF.BANDWIDTH_FIELD_W)
		if (f === null)
			throw new Error('Invalid \'b=\' field')

		this.type = f[1]
		this.bandwidth = f[2]

		this.validate()
	}

	validate()
	{
		// TODO
	}

	toSdp()
	{
		return `b=${this.type}:${this.bandwidth}`
	}
}
xport(BandwidthField)

class StartTime
{
	static PERMANENT = '0'
}
xport(StartTime)

class StopTime
{
	static UNBOUND = '0'
	static PERMANENT = '0'
}
xport(StopTime)

class RepeatField
{
	interval = '0'
	duration = '0'
	offsets = [ '0' ]
}
xport(RepeatField)

class TimeDesc
{
	start = sdp.StartTime.PERMANENT
	stop = sdp.StopTime.PERMANENT
	repeatTimes = []

	constructor(data)
	{
		if (data === undefined || data === null)
			return

		this.from(data)
	}

	from(data)
	{
		if (typeof data === 'string')
			data = data.split('\r\n')

		let l = data.shift()
		if (l === undefined
				|| l[0] !== 't')
			throw new Error('Missing required \'t=\' field')

		let f = l.match(sdp.BNF.TIME_FIELD_W)
		if (f === null)
			throw new Error('Invalid \'t=\' field')

		this.start = f[1]
		this.stop = f[2]

		l = data.shift()
		while (l !== undefined
				&& l[0] === 'r')
		{
			let f = l.match(sdp.BNF.REPEAT_FIELD_W)
			if (f === null)
				break

			let repeat = new sdp.RepeatField()
			repeat.interval = f[1]
			repeat.duration = f[2]
			repeat.offsets = f[3].split(' ')

			this.repeatTimes.push(repeat)

			l = data.shift()
		}

		if (l !== undefined)
			data.unshift(l)

		this.validate()
	}

	validate()
	{
		// TODO
	}

	toSdp()
	{
		return this.toSdpArray().join('\r\n')
	}

	toSdpArray()
	{
		let repeats = this.repeatTimes.map(r => `r=${r.interval} ${r.duration} ${r.offsets.join(' ')}`)
		return [
			`t=${this.start} ${this.stop}`,
			...repeats
		]
	}
}
xport(TimeDesc)

class ZoneAdjustment
{
	time = null
	offset = null
}
xport(ZoneAdjustment)

class ZoneAdjustments
{
	adjustments = []

	constructor(data)
	{
		if (data === undefined
				|| data === null)
			return

		this.from(data)
	}

	from(data)
	{
		let f = data.match(sdp.BNF.ZONE_ADJUSTMENTS_W)
		if (f === null)
			throw new Error('Invalid \'z=\' field')

		let adjs = f[1]

		this.adjustments = adjs.matchAll(/(\S+) (\S+)/g).map(e => {
			let adj = new sdp.ZoneAdjustment()
			adj.time = e[1]
			adj.offset = e[2]
			return adj
		})

		this.validate()
	}

	validate()
	{
		// TODO
	}

	toSdp()
	{
		let adjs = this.adjustments.map(adj => `${adj.time} ${adj.offset}`).join(' ')

		return `z=${adjs}`
	}
}
xport(ZoneAdjustments)

class KeyFieldMethod
{
	static BASE64 = 'base64'
	static CLEAR = 'clear'
	static PROMPT = 'prompt'
	static URI = 'uri'
}
xport(KeyFieldMethod)

class KeyField
{
	method = null
	key = null

	constructor(data)
	{
		if (data === undefined
				|| data === null)
			return

		this.from(data)
	}

	from(data)
	{
		let f = data.match(sdp.BNF.KEY_FIELD_W)
		if (f === null)
			throw new Error('Invalid \'k=\' field')

		f = f.split(':')

		this.method = f[1]
		this.key = f[2] ?? null

		this.validate()
	}

	validate()
	{
		// TODO
	}

	toSdp()
	{
		let key = ''
		if (this.key !== null)
			key = `:${this.key}`

		return `k=${this.method}${key}`
	}
}
xport(KeyField)

class AttributeRTPMapValue
{
	payloadType = null
	encodingName = null
	clockRate = null
	encodingParam = null

	constructor(data)
	{
		if (data === undefined
				|| data === null)
			return

		this.from(data)
	}

	from(data)
	{
		let f = data.match(sdp.BNF.ATTRIBUTE_RTPMAP_VALUE_W)
		if (f === null)
			throw new Error('Invalid rtpmap attribute value')

		this.payloadType = f[1]
		this.encodingName = f[2]
		this.clockRate = f[3]
		this.encodingParam = f[4] ?? null

		this.validate()
	}

	validate()
	{
		// TODO
	}

	toSdp()
	{
		let enc_param = ''
		if (this.encodingParam !== null)
			enc_param = `/${this.encodingParam}`

		return `${this.payloadType} ${this.encodingName}/${this.clockRate}${enc_param}`
	}
}
xport(AttributeRTPMapValue)

class AttributeMIDValue
{
	idTag = null

	constructor(data)
	{
		if (data === undefined
				|| data === null)
			return

		this.from(data)
	}

	from(data)
	{
		let f = data.match(sdp.BNF.ATTRIBUTE_MID_VALUE_W)
		if (f === null)
			throw new Error('Invalid mid attribute value')

		this.idTag = f[1]

		this.validate()
	}

	validate()
	{
		// TODO
	}

	toSdp()
	{
		return `${this.idTag}`
	}
}
xport(AttributeMIDValue)

class AttributeGroupValue
{
	semantics = null
	idTags = []

	constructor(data)
	{
		if (data === undefined
				|| data === null)
			return

		this.from(data)
	}

	from(data)
	{
		let f = data.match(sdp.BNF.ATTRIBUTE_GROUP_VALUE_W)
		if (f === null)
			throw new Error('Invalid group attribute value')

		this.semantics = f[1]
		this.idTags = (f[2] ?? '').split(' ')

		this.validate()
	}

	validate()
	{
		// TODO
	}

	toSdp()
	{
		let id_tags = this.idTags.reduce((acc, id_tag) => `${acc} ${id_tag}`, '')

		return `${this.semantics}${id_tags}`
	}
}
xport(AttributeGroupValue)

class AttributeFieldField
{
	// IANA Session Description Protocol (SDP) Parameters :: att-field

	// RFC4145
	static CONNECTION = 'connection'
	static SETUP = 'setup'

	// RFC4566
	static CAT = 'cat'
	static CHARSET = 'charset'
	static FMTP = 'fmtp'
	static FRAMERATE = 'framerate'
	static INACTIVE = 'inactive'
	static KEYWDS = 'keywds'
	static LANG = 'lang'
	static MAXPTIME = 'maxptime'
	static ORIENT = 'orient'
	static PTIME = 'ptime'
	static RTPMAP = 'rtpmap'
	static RECVONLY = 'recvonly'
	static SDPLANG = 'sdplang'
	static SENDONLY = 'sendonly'
	static SENDRECV = 'sendrecv'
	static QUALITY = 'quality'
	static TOOL = 'tool'
	static TYPE = 'type'

	// RFC4585
	static RTCP_FB = 'rtcp-fb'

	// RFC5576
	static CNAME = 'cname' // source-attr
	// static FMTP = 'fmtp' // source-attr
	static PREVIOUS_SSRC = 'previous-ssrc' // source-attr
	static SSRC = 'ssrc'
	static SSRC_GROUP = 'ssrc-group'

	// RFC5761
	static RTCP_MUX = 'rtcp-mux'

	// RFC5888
	static GROUP = 'group'
	static MID = 'mid'

	// RFC8122
	static FINGERPRINT = 'fingerprint'

	// RFC8285
	static EXTMAP = 'extmap'
	static EXTMAP_ALLOW_MIXED = 'extmap-allow-mixed'

	// RFC8839
	static CANDIDATE = 'candidate'
	static ICE_LITE = 'ice-lite'
	static ICE_MISMATCH = 'ice-mismatch'
	static ICE_OPTIONS = 'ice-options'
	static ICE_PACING = 'ice-pacing'
	static ICE_PWD = 'ice-pwd'
	static ICE_UFRAG = 'ice-ufrag'
	static REMOTE_CANDIDATE = 'remote-candidate'

	// RFC8840
	static END_OF_CANDIDATES = 'end-of-candidates'
}
xport(AttributeFieldField)

class AttributeField
{
	field = null
	value = null

	constructor(data)
	{
		if (data === undefined
				|| data === null)
			return

		this.from(data)
	}

	from(data)
	{
		let f = data.match(sdp.BNF.ATTRIBUTE_FIELD_W)
		if (f === null)
			throw new Error('Invalid \'a=\' field')

		this.field = f[1]
		this.value = f[2] ?? null

		switch (this.field)
		{
			case sdp.AttributeFieldField.RECVONLY:
			case sdp.AttributeFieldField.SENDONLY:
			case sdp.AttributeFieldField.SENDRECV:
			case sdp.AttributeFieldField.END_OF_CANDIDATES:
				this.value = null
				break

			case sdp.AttributeFieldField.RTPMAP:
				this.value = new sdp.AttributeRTPMapValue(this.value)
				break

			case sdp.AttributeFieldField.GROUP:
				this.value = new sdp.AttributeGroupValue(this.value)
				break

			case sdp.AttributeFieldField.MID:
				this.value = new sdp.AttributeMIDValue(this.value)
				break

			default:
				logger.warn(`Unknown Sdp attribute '${this.field}', setting raw value '${this.value}'`)
		}

		this.validate()
	}

	validate()
	{
		// TODO
	}

	toSdp()
	{
		let value = this.value

		if (value !== null)
		{
			if (typeof value === 'object')
				value = value.toSdp()
			value = `:${value}`
		}
		else
			value = ''

		return `a=${this.field}${value}`
	}
}
xport(AttributeField)

class MediaType
{
	// Session Description Protocol (SDP) Parameters :: media

	// RFC4566
	static APPLICATION = 'application'
	static AUDIO = 'audio'
	static MESSAGE = 'message'
	static TEXT = 'text'
	static VIDEO = 'video'

	// RFC6466
	static IMAGE = 'image'
}
xport(MediaType)

class MediaProtoType
{
	// Session Description Protocol (SDP) Parameters :: proto

	// RFC4566
	RTP_AVP = 'RTP/AVP'
	RTP_SAVP = 'RTP/SAVP'
	UDP = 'udp'

	// RFC5764
	static DCCP_TLS_RTP_SAVP = 'DCCP/TLS/RTP/SAVP'
	static DCCP_TLS_RTP_SAVPF = 'DCCP/TLS/RTP/SAVPF'
	static UDP_TLS_RTP_SAVP = 'UDP/TLS/RTP/SAVP'
	static UDP_TLS_RTP_SAVPF = 'UDP/TLS/RTP/SAVPF'
}
xport(MediaProtoType)

class RtpPayloadType
{
	// IANA Real-Time Transport Protocol (RTP) Parameters :: RTP Payload Types (PT) for standard audio and video encodings - Closed

	// RFC3551
	static DYNAMIC_START = '96'
	static DYNAMIC_END = '127'
}
xport(RtpPayloadType)

class RtpPayloadFormatMediaType
{
	// IANA Real-Time Transport Protocol (RTP) Parameters :: RTP Payload Format Media Types

	// RFC7587
	static OPUS = 'opus'
}
xport(RtpPayloadFormatMediaType)

class MediaField
{
	media = null // MediaType
	port = null
	range = null
	proto = null // MediaProtoType
	fmts = [] // RtpPayloadType[]

	constructor(data)
	{
		if (data === undefined
				|| data === null)
			return

		this.from(data)
	}

	from(data)
	{
		let f = data.match(sdp.BNF.MEDIA_FIELD_W)
		if (f === null)
			throw new Error('Invalid \'m=\' field')

		this.media = f[1]
		this.port = f[2]
		this.range = f[3] ?? null
		this.proto = f[4]
		this.fmts = f[5].split(' ')

		this.validate()
	}

	validate()
	{
		// TODO
	}

	toSdp()
	{
		let range = ''
		if (this.range !== null)
			range = `/${this.range}`

		let fmts = this.fmts.join(' ')

		return `m=${this.media} ${this.port}${range} ${this.proto} ${fmts}`
	}
}
xport(MediaField)

class MediaDesc
{
	media = null
	info = null
	conData = null
	bandwidths = []
	key = null
	attrs = []

	constructor(data)
	{
		if (data === undefined || data === null)
			return

		this.from(data)
	}

	from(data)
	{
		if (typeof data === 'string')
			data = data.split('\r\n')

		let l = data.shift()
		if (l === undefined
				|| l[0] !== 'm')
			throw new Error('Missing required \'m=\' field')

		this.media = new sdp.MediaField(l)

		l = data.shift()
		if (l !== undefined
				&& l[0] === 'i')
		{
			f = l.match(sdp.BNF.INFORMATION_FIELD_W)
			if (f === null)
				throw new Error('Invalid \'i=\' field')
			this.info = f[1]

			l = data.shift()
		}

		if (l !== undefined
				&& l[0] === 'c')
		{
			this.conData = new sdp.ConnectionField(l)

			l = data.shift()
		}

		while (l !== undefined
				&& l[0] === 'b')
		{
			this.bandwidths.push(new sdp.BandwidthField(l))

			l = data.shift()
		}

		if (l !== undefined
				&& l[0] === 'k')
		{
			this.key = new sdp.KeyField(l)

			l = data.shift()
		}

		while (l !== undefined
				&& l[0] === 'a')
		{
			this.attrs.push(new sdp.AttributeField(l))

			l = data.shift()
		}

		if (l !== undefined)
			data.unshift(l)

		this.validate()
	}

	validate()
	{
		// TODO
	}

	toSdp()
	{
		return this.toSdpArray().join('\r\n')
	}

	toSdpArray()
	{
		let data = []

		this.validate()

		data.push(this.media.toSdp())

		if (this.info !== null)
			data.push(`i=${this.info}`)

		if (this.conData !== null)
			data.push(this.conData.toSdp())

		for (let bandwidth of this.bandwidths)
			data.push(bandwidth.toSdp())

		if (this.key !== null)
			data.push(this.key.toSdp())

		for (let attr of this.attrs)
			data.push(attr.toSdp())

		return data
	}
}
xport(MediaDesc)
