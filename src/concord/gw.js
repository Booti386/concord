let logger = await use('concord.logger')
let ws = await use('concord.ws')

let Bitfield = await use('concord.types.Bitfield')
let Enum = await use('concord.types.Enum')

let gw = await ready('concord.gw')

class Op extends Enum
{
	static DISPATCH = 0
	static HEARTBEAT = 1
	static IDENT = 2
	static UPD_STATUS = 3
	static UPD_VOICE_STATE = 4
	static RESUME = 6
	static RECONNECT = 7
	static REQUEST_GUILD_MEMBERS = 8
	static INVAL_SESSION = 9
	static HELLO = 10
	static HEARTBEAT_ACK = 11
}
xport(Op)

class CloseEventCode extends ws.CloseEventCode
{
	static UNK_ERROR = 4000
	static UNK_OPCODE = 4001
	static DECODE_ERR = 4002
	static NOT_AUTH = 4003
	static AUTH_FAILED = 4004
	static ALREADY_AUTH = 4005
	static INVAL_SEQ = 4007
	static RATE_LIMITED = 4008
	static SESSION_TIMEOUT = 4009
	static INVAL_SHARD = 4010
	static SHARD_REQ = 4011
	static INVAL_API_VER = 4012
	static INVAL_INTENT = 4013
	static DISALLOWED_INTENT = 4014
}
xport(CloseEventCode)

class Intent extends Bitfield
{
	static NONE = 0
	static GUILDS = 1 << 0
	static GUILD_MEMBERS = 1 << 1
	static GUILD_BANS = 1 << 2
	static GUILD_EMOJIS = 1 << 3
	static GUILD_INTEGRATIONS = 1 << 4
	static GUILD_WEBHOOKS = 1 << 5
	static GUILD_INVITES = 1 << 6
	static GUILD_VOICE_STATES = 1 << 7
	static GUILD_PRESENCES = 1 << 8
	static GUILD_MESSAGES = 1 << 9
	static GUILD_MESSAGE_REACTIONS = 1 << 10
	static GUILD_MESSAGE_TYPING = 1 << 11
	static DIRECT_MESSAGES = 1 << 12
	static DIRECT_MESSAGE_REACTIONS = 1 << 13
	static DIRECT_MESSAGE_TYPING = 1 << 14
	static MESSAGE_CONTENT = 1 << 15
	static GUILD_SCHEDULED_EVENTS = 1 << 16
	static AUTO_MODERATION_CONFIGURATION = 1 << 20
	static AUTO_MODERATION_EXECUTION = 1 << 21
}
xport(Intent)

class PresenceStatus extends Enum
{
	static ONLINE = 'online'
	static DND = 'dnd'
	static IDLE = 'idle'
	static INVISIBLE = 'invisible'
	static OFFLINE = 'offline'
}
xport(PresenceStatus)

class ActivityType extends Enum
{
	static GAME = 0
	static STREAMING = 1
	static LISTENING = 2
	static CUSTOM = 3
}
xport(ActivityType)

class Activity
{
	type       // ActivityType
	name       // String
	created_at // unix_ts
	/* ... */

	constructor(type, name, created_at)
	{
		let now = Date.now()

		if (type === undefined)
			type = gw.ActivityType.GAME
		if (name === undefined)
			name = ''
		if (created_at === undefined)
			created_at = now

		this.type = type
		this.name = name
		this.created_at = created_at
	}
}
xport(Activity)

class Presence
{
	status // PresenceStatus
	afk    // bool
	since  // unix_ts
	game   // Activity

	constructor(status, afk, since, game)
	{
		if (status === undefined)
			status = gw.PresenceStatus.ONLINE
		if (afk === undefined)
			afk = false
		if (since === undefined)
			since = null
		if (game === undefined)
			game = null

		this.status = status
		this.afk = afk
		this.since = since
		this.game = game
	}
}
xport(Presence)

class Gw extends ws.Ws
{
	static API_VER = 8
	static API_ROOT = 'https://discord.com/api'

	lastSeq = null
	idUser = null
	idSession = null
	heartbeatInterval = null
	heartbeatIntervalId = null
	token = null
	identProps = {
		os: '',
		browser: '',
		device: '',
		browser_user_agent: '',
		browser_version: '',
		os_version: '',
		referrer: '',
		referring_domain: '',
		referrer_current: '',
		referring_domain_current: ''
	}
	intents = gw.Intent.GUILDS
	presence = new gw.Presence()

	constructor(token, auto_reconnect_timeout)
	{
		super(null, auto_reconnect_timeout)

		this.token = token
	}

	boot()
	{
		let rq = fetch(`${Gw.API_ROOT}/gateway`)
		rq.then(ans => ans.json())
				.then(data => super.boot(`${data.url}?v=${Gw.API_VER}&encoding=json`))
	}

	updPresence(presence)
	{
		this.presence = presence
		this.sendUpdStatus()
	}

	send(op, data)
	{
		let op_msg = gw.Op.asString(op)
		let d = {
			op: op
		}

		if (data !== undefined)
			d.d = data

		d = JSON.stringify(d)

		logger.debug(`Gw.send(op = ${op_msg}): ${d}`)

		this.ws.send(d)
	}

	sendIdent()
	{
		return this.send(gw.Op.IDENT, {
			token: this.token,
			intents: this.intents,
			properties: this.identProps,
			presence: this.presence
		})
	}

	sendUpdStatus()
	{
		return this.send(gw.Op.UPD_STATUS, this.presence)
	}

	sendUpdVoiceState(id_guild, id_channel, self_mute, self_deaf)
	{
		if (id_channel === undefined)
			id_channel = null
		if (self_mute === undefined)
			self_mute = false
		if (self_deaf === undefined)
			self_deaf = false

		return this.send(gw.Op.UPD_VOICE_STATE, {
			guild_id: id_guild,
			channel_id: id_channel,
			self_mute: self_mute,
			self_deaf: self_deaf
		})
	}

	sendResume()
	{
		return this.send(gw.Op.RESUME, {
			token: this.token,
			session_id: this.idSession,
			seq: this.lastSeq
		})
	}

	sendHeartbeat()
	{
		return this.send(gw.Op.HEARTBEAT, this.lastSeq)
	}

	onWsMsg(ev)
	{
		super.onWsMsg(ev)

		let data

		try
		{
			data = JSON.parse(ev.data)
		}
		catch (ex)
		{
			throw new Error(`Invalid message: ${ev.data}`)
		}

		let op_msg = gw.Op.asString(data.op)

		logger.debug(`Gw.onWsMsg(): op = ${op_msg}`)

		switch (data.op)
		{
			case gw.Op.DISPATCH: this.onMsgDispatch(data); break
			case gw.Op.RECONNECT: this.onMsgReconnect(data); break
			case gw.Op.INVAL_SESSION: this.onMsgInvalSession(data); break
			case gw.Op.HELLO: this.onMsgHello(data); break
			case gw.Op.HEARTBEAT_ACK: this.onMsgHeartbeatAck(data); break
			default:
				logger.debug(`Gw.onWsMsg(): Message not handled`)
				break
		}
	}

	onWsClose(ev)
	{
		super.onWsClose(ev)

		let close_msg = gw.CloseEventCode.asString(ev.code)

		let reason = ev.reason
		if (reason !== '')
			reason = `: ${reason}`

		logger.debug(`Gw.onWsClose(): ${close_msg}${reason}`)

		window.clearInterval(this.heartbeatIntervalId)
	}

	onMsgDispatch(data)
	{
		let d = data.d

		logger.debug(`Gw.onMsgDispatch(): t = ${data.t}`)

		this.lastSeq = data.s

		const map = {
			CHANNEL_CREATE: this.onChannelCreate,
			CHANNEL_DELETE: this.onChannelDelete,
			CHANNEL_UPDATE: this.onChannelUpdate,
			GUILD_CREATE: this.onGuildCreate,
			MESSAGE_CREATE: this.onMessageCreate,
			MESSAGE_REACTION_ADD: this.onMessageReactionAdd,
			READY: this.onReady,
			VOICE_SERVER_UPDATE: this.onVoiceServerUpdate,
			VOICE_STATE_UPDATE: this.onVoiceStateUpdate
		}

		let handler = map[data.t] ?? null
		if (handler === null)
		{
			logger.debug(`Gw.onMsgDispatch(): Dispatch message not handled`)
			return
		}

		handler.call(this, d)
	}

	onMsgReconnect(data)
	{
		logger.debug('Gw.onMsgReconnect()')

		this.sendResume()
	}

	onMsgInvalSession(data)
	{
		let delay = 5000

		logger.debug('Gw.onMsgInvalSession()')

		window.setTimeout(() => {
			logger.debug(`Gw.onMsgInvalSession(): Attempting to identify again in ${delay} ms...`)
			window.setTimeout(() => this.sendIdent(), delay)
		}, 0)
	}

	onMsgHello(data)
	{
		let d = data.d

		logger.debug('Gw.onMsgHello()')
		this.heartbeatInterval = d.heartbeat_interval

		if (this.heartbeatIntervalId !== null)
			window.clearInterval(this.heartbeatIntervalId)

		this.heartbeatIntervalId = window.setInterval(() => {
			this.sendHeartbeat()
		}, this.heartbeatInterval)

		this.sendIdent()
	}

	onMsgHeartbeatAck(data)
	{
		logger.debug('Gw.onMsgHeartbeatAck()')
	}

	onChannelCreate(chan)
	{
		logger.debug('Gw.onChannelCreate(): ', chan)
	}

	onChannelDelete(chan)
	{
		logger.debug('Gw.onChannelDelete(): ', chan)
	}

	onChannelUpdate(chan)
	{
		logger.debug('Gw.onChannelUpdate(): ', chan)
	}

	onGuildCreate(guild)
	{
		logger.debug('Gw.onGuildCreate(): ', guild)

		guild.voice_states ??= []

		for (let state of guild.voice_states)
		{
			state.guild_id = guild.id
			this.onVoiceStateUpdate(state)
		}
	}

	onMessageCreate(msg)
	{
		logger.debug('Gw.onMessageCreate(): ', msg)
	}

	onMessageReactionAdd(react)
	{
		logger.debug('Gw.onMessageReactionAdd(): ', react)
	}

	onReady(payload)
	{
		logger.debug('Gw.onReady(): ', payload)

		this.idUser = payload.user.id
		this.idSession = payload.session_id

		for (let guild of payload.guilds)
			this.onGuildCreate(guild)
	}

	onVoiceServerUpdate(state)
	{
		logger.debug('Gw.onVoiceServerUpdate(): ', state)
	}

	onVoiceStateUpdate(state)
	{
		logger.debug('Gw.onVoiceStateUpdate(): ', state)
	}
}
xport(Gw)
